var unreadCount = 0;
var socket = io();

socket.on('new-notification', function(data){
    unreadCount++;
    $('#notification-count').html(unreadCount);
    $('#panel-notification-count').html(unreadCount);
    $('#notifications').prepend('<li class="list-group-item row"><div class="col-xs-2"><img src="./public/pics/'+data.p+'" class="img-responsive img-circle"></div><div class="col-xs-10"><span>'+data.u+'&nbsp;</span>'+data.i+'</div></li>');
    $('#notifications li:first-child').slideDown("slow").css({display:"block"});
});

socket.on('unread-notification', function(data){
    $('#notifications').prepend('<li class="list-group-item row" style="display:block"><div class="col-xs-2"><img src="./public/pics/'+data.p+'" class="img-responsive img-circle"></div><div class="col-xs-10"><span>'+data.u+'&nbsp;</span>'+data.i+'</div></li>');
    
});

socket.on('unread-count', function(data){
    unreadCount = data;
    $('#notification-count').html(unreadCount);
    $('#panel-notification-count').html(unreadCount);
});

$('#bell').click(function(){
    socket.emit('markAllAsRead');
    var x = $('#notification-panel').css("display");
    if(x == 'none'){
        $('#notification-panel').slideDown("slow").css({display:"block"});
    }else{
        $('#notification-panel').slideUp("slow",function(){
            $(this).css({display:"none"});
            $('#notifications').empty();
        });
    }
});
