var express = require("express");
var logger = require('connect-logger');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var db = require(__dirname + "/modules/db.js");
var ng = require(__dirname + "/modules/notification-generator.js");

app.use(logger());
app.use('/public', express.static('public'));

app.get('/',function(req,res){
  res.sendFile(__dirname+"/views/index.html");
});

setInterval(function(){
  var notification = ng.randomNotificationGenerator();
  db.newNotification(notification);
  io.emit('new-notification',notification);
},ng.randomNumberGenerator(2000,5000));


io.on('connection', function(socket){
  db.sendUnreadCount(io);
  db.sendAllUnreadNotifications(io);
  socket.on('markAllAsRead', function(){
    db.markAllAsRead();
    db.sendUnreadCount(io);
  });
});

http.listen(process.env.PORT);