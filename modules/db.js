var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'sql6.freemysqlhosting.net',
    user     : 'sql6105945',
    password : 'yzAV8HZKeN',
    database : 'sql6105945'
});

connection.connect();

var module = module.exports = {};

module.newNotification = function(data){
    connection.query("INSERT INTO notifications (name , notification, pic) VALUES ('"+data.u+"','"+data.i+"','"+data.p+"')", function(error, rows, fields) {
        if (error){
            return console.log(error);
        }
        //console.dir(rows);
    });
};

module.markAllAsRead = function(){
    connection.query("UPDATE notifications SET status = 1", function(error, rows, fields) {
        if (error){
            return console.log(error);
        }
    });
};

module.getAllNotifications = function(){
    connection.query("SELECT * FROM notifications", function(error, rows, fields) {
        if (error){
            return console.log(error);
        }
        //console.dir(rows);
    });
};

module.sendAllUnreadNotifications = function(io){
    connection.query("SELECT * FROM notifications WHERE status = 0", function(error, rows, fields) {
        if (error){
            return console.log(error);
        }
        for(var j = 0; j < rows.length; j++){
            var notification = {
                u : rows[j].name,
                i : rows[j].notification,
                p : rows[j].pic
                
            };
            io.emit('unread-notification',notification);
        }
    });
};

module.sendUnreadCount = function(io){
    connection.query("SELECT COUNT(*) AS count FROM notifications WHERE status = 0", function(error, rows, fields) {
        if (error){
            return console.log(error);
        }
        io.emit('unread-count',rows[0].count);
    });
};