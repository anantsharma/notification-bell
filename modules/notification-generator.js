var module = module.exports = {};

var users = ['Rahul Benjamin','John Smith','Erdo Klein','Pete Ray'];
var items = ['posted a photo on your wall','commented on your post','liked your post','commented on your last video'];

module.randomNumberGenerator = function(low, high) {
  return Math.floor(Math.random() * (high - low + 1) + low);
};

function getPic(k){
    var res = '';
    switch(k){
        case 0: res = 'rahul.png';break;
        case 1: res = 'john.png';break;
        case 2: res = 'erdo.png';break;
        case 3: res = 'pete.png';break;
    }
    return res;
}

module.randomNotificationGenerator = function(){
    var i = items[module.randomNumberGenerator(0, items.length - 1)];
    var k = module.randomNumberGenerator(0, users.length - 1);
    var u = users[k];
    var p = getPic(k);
    var notification = {u:u, i:i, p:p};
    return notification;
};